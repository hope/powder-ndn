# powder-ndn
This repository contains code for implementing NDN (Named Data Networking) on the University of Utah's POWDER testbed. Here is the link to the paper repository written about the research conducted with this repository. https://gitlab.flux.utah.edu/hope/paper-template

Look at Getting Started for details on setting up.

**Resources:**
* [https://named-data.net/doc/NFD/current/](https://named-data.net/doc/NFD/current/)
* [http://named-data.net/codebase/platform/documentation/](http://named-data.net/codebase/platform/documentation/)
* [http://named-data.net/doc/NLSR/current/](http://named-data.net/doc/NLSR/current/)

